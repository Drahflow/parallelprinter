#ifndef H_1AB2570B_E8DE_479B_A21A_D41F54531F00
#define H_1AB2570B_E8DE_479B_A21A_D41F54531F00

#include <stdint.h>

void beep_synchronous(uint32_t ms, uint32_t freq);
extern uint32_t beep_error;

#endif

#include "simulation.c"

#include <assert.h>

void testCalculateStrutForces() {
  // Displacement platform = { 0, 0, 0 };
  Displacement centerOfMass = { 0, 0, 0 };
  Displacement attachmentTarget[] = {
    { 0, 0, 2},
    { -2, -1, 1},
    { 2, -1, 1},
    { -2, 1, 1},
    { 2, 1, 1},
    { 1, -2, 1},
    { 1, 2, 1},
  };
  Displacement sliderPositions[] = {
    { 0, 0, 3},
    { -3, -1, 1},
    { 3, -1, 1},
    { -3, 1, 1},
    { 3, 1, 1},
    { 1, -3, 1},
    { 1, 3, 1},
  };

  double strutLength[MAIN_AXIS_COUNT];
  for(int i = 0; i < MAIN_AXIS_COUNT; ++i) {
    Displacement dir = displacementSub(sliderPositions[i], attachmentTarget[i]);
    strutLength[i] = sqrt(displacementDot(dir, dir));
  }

  double forces[MAIN_AXIS_COUNT];
  platformGravitationalForce = 15;

  assert(calculateStrutForces(centerOfMass, attachmentTarget, sliderPositions, strutLength, forces));

  assert(-15.001 < forces[0] && forces[0] < -14.999);
  assert(-0.001 < forces[1] && forces[1] < 0.001);
  assert(-0.001 < forces[2] && forces[2] < 0.001);
  assert(-0.001 < forces[3] && forces[3] < 0.001);
  assert(-0.001 < forces[4] && forces[4] < 0.001);
  assert(-0.001 < forces[5] && forces[5] < 0.001);
  assert(-0.001 < forces[6] && forces[6] < 0.001);
}

void testCalculateStrutForces2() {
  // Displacement platform = { 0, 0, 0 };
  Displacement centerOfMass = { 0, 0.1, -10 };
  Displacement attachmentTarget[] = {
    { 0, 0.1, 2},
    { 0, -0.1, 2},
    { 2, -1, 1},
    { -2, 1, 1},
    { 2, 1, 1},
    { 1, -2, 1},
    { 1, 2, 1},
  };
  Displacement sliderPositions[] = {
    { 0, 0.1, 3},
    { 0, -0.1, 3},
    { 3, -1, 1},
    { -3, 1, 1},
    { 3, 1, 1},
    { 1, -3, 1},
    { 1, 3, 1},
  };

  double strutLength[MAIN_AXIS_COUNT];
  for(int i = 0; i < MAIN_AXIS_COUNT; ++i) {
    Displacement dir = displacementSub(sliderPositions[i], attachmentTarget[i]);
    strutLength[i] = sqrt(displacementDot(dir, dir));
  }

  double forces[MAIN_AXIS_COUNT];

  assert(calculateStrutForces(centerOfMass, attachmentTarget, sliderPositions, strutLength, forces));

  for(int i = 0; i < MAIN_AXIS_COUNT; ++i) {
    printf("Force at strut %d: %lf\n", i, forces[i]);
  }
  assert(-7.6 < forces[0] && forces[0] < -7.4);
  assert(-7.6 < forces[1] && forces[1] < -7.4);
  assert(forces[1] < forces[0]);
  assert(-0.1 < forces[2] && forces[2] < 0.1);
  assert(-0.1 < forces[3] && forces[3] < 0.1);
  assert(-0.1 < forces[4] && forces[4] < 0.1);
  assert(-0.1 < forces[5] && forces[5] < 0.1);
  assert(-0.1 < forces[6] && forces[6] < 0.1);
}

static void setupTestConfig() {
  initMotorDrivers();

  for(int i = 0; i < MAIN_AXIS_COUNT; ++i) {
    motors[i].pos = 0;
    sliderZero[i].x = (i - 3) * 100.0;

    sliderUpStep[i].x = 0.0;
    sliderUpStep[i].y = 0.0;
    sliderUpStep[i].z = 1.75 / 256 / 200;

    upperLimit[i] = 200 * 256 * 200 / 1.75;
    lowerLimit[i] = -700 * 256 * 200 / 1.75;
  }

  strutLength[0] = 516.0;
  strutLength[1] = 516.0;
  strutLength[2] = 516.0;
  strutLength[3] = 516.0;
  strutLength[4] = 516.0;
  strutLength[5] = 516.0;
  strutLength[6] = 516.0;

  sliderZero[0].y = -27.0;
  sliderZero[1].y = -27.0;
  sliderZero[2].y = -27.0;
  sliderZero[3].y = -27.0;
  sliderZero[4].y = -27.0;
  sliderZero[5].y = -27.0;
  sliderZero[6].y = -27.0;

  sliderZero[0].z = -75.0;
  sliderZero[1].z = -34.0;
  sliderZero[2].z = -119.0;
  sliderZero[3].z = -79.0;
  sliderZero[4].z = -119.0;
  sliderZero[5].z = -34.0;
  sliderZero[6].z = -75.0;

  sliderBackslash[0] = 1.0;
  sliderBackslash[1] = 1.0;
  sliderBackslash[2] = 1.0;
  sliderBackslash[3] = 1.0;
  sliderBackslash[4] = 1.0;
  sliderBackslash[5] = 1.0;
  sliderBackslash[6] = 1.0;

  sliderElasticity[0] = 0.5;
  sliderElasticity[1] = 0.5;
  sliderElasticity[2] = 0.5;
  sliderElasticity[3] = 0.5;
  sliderElasticity[4] = 0.5;
  sliderElasticity[5] = 0.5;
  sliderElasticity[6] = 0.5;

  platformAttachment[0].x = 40.0 - 3 * 100.0;
  platformAttachment[1].x = 25.0 - 2 * 100.0;
  platformAttachment[2].x = 5.0 - 1 * 100.0;
  platformAttachment[3].x = 0.0 - 0 * 100.0;
  platformAttachment[4].x = -5.0 + 1 * 100.0;
  platformAttachment[5].x = -25.0 + 2 * 100.0;
  platformAttachment[6].x = -40.0 + 3 * 100.0;

  platformAttachment[0].y = 5.0 - 75.0 - 21.0; // 21.0 == GimbalLowerBrace.BaseAxle.Sketch026.GimbalLowerBraceOffset
  platformAttachment[1].y = 0.0 - 75.0 - 21.0;
  platformAttachment[2].y = 10.0 - 75.0 - 21.0;
  platformAttachment[3].y = 50.0 - 75.0 - 21.0;
  platformAttachment[4].y = 10.0 - 75.0 - 21.0;
  platformAttachment[5].y = 0.0 - 75.0 - 21.0;
  platformAttachment[6].y = 5.0 - 75.0 - 21.0;

  platformAttachment[0].z = 0.0;
  platformAttachment[1].z = 40.0;
  platformAttachment[2].z = -45.0;
  platformAttachment[3].z = 0.0;
  platformAttachment[4].z = -45.0;
  platformAttachment[5].z = 40.0;
  platformAttachment[6].z = 0.0;

  kinematicsSubdivisionInterval = 0.05;
  maximumStepsPerSecond = 200000;
  rotationSpeed = 0.3;
  movementSpeed = 1;

  motorSchedule = NULL;

  Position somewhereBelow = {
    {0.0, 75.0, -590.0},
    {1.0, 0.0, 0.0, 0.0}
  };

  setZero(somewhereBelow);
}

static void runKinematicsABit() {
  Position delta = {
    {12.0, 34.0, 56.0},
    {0.9205048534524404, 0.3907311284892737, 0, 0}
  };
  Position base = {
    {11.0, 22.0, 33.0},
    {0.7431448254773942, 0, 0, -0.6691306063588582}
  };
  Rotation unit1 = quaternionMul(unitQuaternionInverse(base.rot), base.rot);
  Rotation unit2 = quaternionMul(base.rot, unitQuaternionInverse(base.rot));
  printf("unit1: @ %lf %lf %lf %lf\n", unit1.r, unit1.i, unit1.j, unit1.k);
  printf("unit2: @ %lf %lf %lf %lf\n", unit2.r, unit2.i, unit2.j, unit2.k);

  Position addSub = relativePositionAdd(relativePositionSub(base, delta), delta);
  Position subAdd = relativePositionSub(relativePositionAdd(base, delta), delta);

  printf("addSub: %lf %lf %lf @ %lf %lf %lf %lf\n",
      addSub.disp.x, addSub.disp.y, addSub.disp.z,
      addSub.rot.r, addSub.rot.i, addSub.rot.j, addSub.rot.k);
  printf("subAdd: %lf %lf %lf @ %lf %lf %lf %lf\n",
      subAdd.disp.x, subAdd.disp.y, subAdd.disp.z,
      subAdd.rot.r, subAdd.rot.i, subAdd.rot.j, subAdd.rot.k);

  setupTestConfig();

  moveAgain();

  uint64_t step = 0;
  while(
      motorsMoving()
  ) {
    if(++step % 20000 == 0) {
      for(int i = 0; i < MOTOR_COUNT; ++i) printf("%d ", motors[i].pos);
      printf(" Tool: %lf %lf %lf @ %lf %lf %lf %lf\n",
          tool.disp.x, tool.disp.y, tool.disp.z, tool.rot.r, tool.rot.i, tool.rot.j, tool.rot.k);
    }

    runKinematics();
    simulate();
  }

  Position toolAtCenter = {
    {0.0, 30.0, -45.0},
    {1.0, 0.0, 0.0, 0.0}
  };
  setToolAttachedAt(toolAtCenter);

  Position targetPosition = {
    {0.0, 105.0, -635.0},
    {0.9238795325112867, -0.3826834323650898, 0, 0},
  };
  moveTo(targetPosition);

  step = 0;
  while(
      tool.disp.x != targetPosition.disp.x ||
      tool.disp.y != targetPosition.disp.y ||
      tool.disp.z != targetPosition.disp.z ||
      tool.rot.r != targetPosition.rot.r ||
      tool.rot.i != targetPosition.rot.i ||
      tool.rot.j != targetPosition.rot.j ||
      tool.rot.k != targetPosition.rot.k ||
      motorsMoving()
  ) {
    if(++step % 20000 == 0) {
      for(int i = 0; i < MOTOR_COUNT; ++i) printf("%d ", motors[i].pos);
      printf(" Tool: %lf %lf %lf @ %lf %lf %lf %lf\n",
          tool.disp.x, tool.disp.y, tool.disp.z, tool.rot.r, tool.rot.i, tool.rot.j, tool.rot.k);
    }

    runKinematics();
    simulate();
  }

  Position targetPosition2 = {
    {0.0, 105.0, -635.0},
    {1, 0, 0, 0},
  };
  moveTo(targetPosition2);

  step = 0;
  while(
      tool.disp.x != targetPosition2.disp.x ||
      tool.disp.y != targetPosition2.disp.y ||
      tool.disp.z != targetPosition2.disp.z ||
      tool.rot.r != targetPosition2.rot.r ||
      tool.rot.i != targetPosition2.rot.i ||
      tool.rot.j != targetPosition2.rot.j ||
      tool.rot.k != targetPosition2.rot.k ||
      motorsMoving()
  ) {
    if(++step % 20000 == 0) {
      for(int i = 0; i < MOTOR_COUNT; ++i) printf("%d ", motors[i].pos);
      printf(" Tool: %lf %lf %lf @ %lf %lf %lf %lf\n",
          tool.disp.x, tool.disp.y, tool.disp.z, tool.rot.r, tool.rot.i, tool.rot.j, tool.rot.k);
    }

    runKinematics();
    simulate();
  }
}

static void queueToolMoveCommands() {
  setupTestConfig();

  Position toolAtCenter = {
    {0.0, 30.0, -45.0},
    {1.0, 0.0, 0.0, 0.0}
  };
  setToolAttachedAt(toolAtCenter);

  Position targetPositionA = {
    {0.0, 155.0, -635.0},
    {1, 0, 0, 0},
  };
  // moveTo(targetPositionA);

  // Position targetPositionB = {
  //   {0.0, 155.0, -605.0},
  //   {1, 0, 0, 0},
  // };
  // moveTo(targetPositionB);

  Position targetPositionC = {
    {30.0, 155.0, -550.0},
    {1, 0, 0, 0},
  };
  moveTo(targetPositionC);

  int step = 0;
  while(
      tool.disp.x != targetPositionA.disp.x ||
      tool.disp.y != targetPositionA.disp.y ||
      tool.disp.z != targetPositionA.disp.z ||
      tool.rot.r != targetPositionA.rot.r ||
      tool.rot.i != targetPositionA.rot.i ||
      tool.rot.j != targetPositionA.rot.j ||
      tool.rot.k != targetPositionA.rot.k ||
      motorsMoving()
  ) {
    if(++step % 20000 == 0) {
      for(int i = 0; i < MOTOR_COUNT; ++i) printf("%d ", motors[i].pos);
      printf(" Tool: %lf %lf %lf @ %lf %lf %lf %lf\n",
          tool.disp.x, tool.disp.y, tool.disp.z, tool.rot.r, tool.rot.i, tool.rot.j, tool.rot.k);
    }

    runKinematics();
    simulate();
  }
}

int main(void) {
  testCalculateStrutForces();
  testCalculateStrutForces2();

  queueToolMoveCommands();
  // runKinematicsABit();
}

#ifndef H_E6507552_4F87_4729_A124_CB8197EC7121
#define H_E6507552_4F87_4729_A124_CB8197EC7121

constexpr int videoWidth = 640;
constexpr int videoHeight = 480;
constexpr int mainAxisCount = 7;

// #define DEBUG_INTERCEPTIONS
// #define DEBUG_FRAMETIMINGS

#endif

#ifndef H_8876CFAA_AFBE_4D29_BB1D_6038F1068014
#define H_8876CFAA_AFBE_4D29_BB1D_6038F1068014

#include <cstdint>

uint64_t now(); // nanoseconds on some monotonic clock

#endif
